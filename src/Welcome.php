<?php

namespace Gdoters\Platform;

class Welcome
{
  public function greet()
  {
    return 'Hi!, How are you doing today?';
  }
}