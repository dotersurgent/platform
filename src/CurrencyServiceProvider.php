<?php

namespace Gdoters\Platform;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class CurrencyServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerCurrency();

		if ($this->app->runningInConsole()) {
			$this->registerResources();
			$this->registerCurrencyCommands();
		}
	}

	/**
	 * Register currency provider.
	 *
	 * @return void
	 */
	public function registerCurrency()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/../config/core/currency.php', 'core.currency'
		);

		$this->app->singleton('currency', function ($app) {
			return new Currency(
				config('core.currency'),
				$app['cache']
			);
		});
	}

	/**
	 * Register currency resources.
	 *
	 * @return void
	 */
	public function registerResources()
	{
		if ($this->isLumen() === false) {
			$this->publishes([
				__DIR__ . '/../config/core/currency.php' => config_path('core/currency.php'),
			], 'config');
		}

		$this->publishes([
			__DIR__ . '/../database/migrations/currency' => base_path('/database/migrations'),
		], 'migrations');
	}

	/**
	 * Register currency commands.
	 *
	 * @return void
	 */
	public function registerCurrencyCommands()
	{
		$this->commands([
			Console\Currency\Cleanup::class,
			Console\Currency\Manage::class,
			Console\Currency\Update::class,
		]);
	}

	/**
	 * Check if package is running under Lumen app
	 *
	 * @return bool
	 */
	protected function isLumen()
	{
		return Str::contains($this->app->version(), 'Lumen') === true;
	}
}
