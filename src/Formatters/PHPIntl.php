<?php

namespace Gdoters\Platform\Formatters;

use NumberFormatter;
use Gdoters\Platform\Contracts\FormatterInterface;

class PHPIntl implements FormatterInterface
{
	/**
	 * Number formatter instance.
	 *
	 * @var NumberFormatter
	 */
	protected $formatter;

	/**
	 * Create a new instance.
	 */
	public function __construct()
	{
		$this->formatter = new NumberFormatter(config('app.locale'), NumberFormatter::CURRENCY);
	}

	/**
	 * {@inheritdoc}
	 */
	public function format($value, $code = null)
	{
		$format = $this->formatter;
    $format->setTextAttribute(NumberFormatter::CURRENCY_CODE, $code);
    $format->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
    $format->setPattern( str_replace('¤#','¤ #', $format->getPattern() ) );
    return $format->formatCurrency($value, $code);
	}
}