<?php

namespace Gdoters\Platform\Console\Provider;

use Illuminate\Support\Arr;
use Illuminate\Console\Command;

class Manage extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'provider:manage
	{action : Action to perform (add, update, or delete)}
	{provider : Code or comma separated list of codes for providers}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Manage provider values';

	/**
	 * Currency storage instance
	 *
	 * @var \Gdoters\Platform\Contracts\Currency\DriverInterface
	 */
	protected $storage;

	/**
	 * All installable providers.
	 *
	 * @var array
	 */
	protected $providers;

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		$this->storage = app('provider')->getDriver();
		$this->providers = config('core.provider');

		parent::__construct();
	}

	/**
	 * Execute the console command for Laravel 5.4 and below
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->handle();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$action = $this->getActionArgument(['add', 'update', 'delete']);

		foreach ($this->getProviderArgument() as $provider) {
			$this->$action(strtolower($provider));
		}
	}

	/**
	 * Add provider to storage.
	 *
	 * @param string $provider
	 *
	 * @return void
	 */
	protected function add($provider)
	{
		if (($data = $this->getProvider($provider)) === null) {
			$this->error("Provider \"{$provider}\" not found");
			return;
		}

		$this->output->write("Adding Provider {$provider}...");

		if (is_string($result = $this->storage->create((new $data($provider))->toDatabase()))) {
			$this->output->writeln('<error>' . ($result ?: 'Failed') . '</error>');
		} else {
			$this->output->writeln("<info>success</info>");
		}
	}

	/**
	 * Update provider in storage.
	 *
	 * @param string $provider
	 *
	 * @return void
	 */
	protected function update($provider)
	{
		if (($data = $this->getProvider($provider)) === null) {
			$this->error("Provider \"{$provider}\" not found");
			return;
		}

		$this->output->write("Updating {$provider} Provider...");

		if (is_string($result = $this->storage->update($provider, (new $data($provider))->toDatabase()))) {
			$this->output->writeln('<error>' . ($result ?: 'Failed') . '</error>');
		} else {
			$this->output->writeln("<info>success</info>");
		}
	}

	/**
	 * Delete provider from storage.
	 *
	 * @param string $provider
	 *
	 * @return void
	 */
	protected function delete($provider)
	{
		$this->output->write("Deleting {$provider} Provider...");

		if (is_string($result = $this->storage->delete($provider))) {
			$this->output->writeln('<error>' . ($result ?: 'Failed') . '</error>');
		} else {
			$this->output->writeln("<info>success</info>");
		}
	}

	/**
	 * Get provider argument.
	 *
	 * @return array
	 */
	protected function getProviderArgument()
	{
		// Get the user entered value
		$value = preg_replace('/\s+/', '', $this->argument('provider'));
		// Return all providers if requested
		if ($value === 'all') {
			return array_keys($this->providers);
		}

		return explode(',', $value);
	}

	/**
	 * Get action argument.
	 *
	 * @param array $validActions
	 *
	 * @return array
	 */
	protected function getActionArgument($validActions = [])
	{
		$action = strtolower($this->argument('action'));

		if (in_array($action, $validActions) === false) {
			throw new \RuntimeException("The \"{$action}\" option does not exist.");
		}

		return $action;
	}

	/**
	 * Get currency data.
	 *
	 * @param string $provider
	 *
	 * @return array
	 */
	protected function getProvider($provider)
	{
		return Arr::get($this->providers['providers'], $provider);
	}
}
