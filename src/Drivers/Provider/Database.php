<?php

namespace Gdoters\Platform\Drivers\Provider;

use DateTime;
use Illuminate\Support\Collection;
use Illuminate\Database\DatabaseManager;
use Gdoters\Platform\Drivers\AbstractDriver;

class Database extends AbstractDriver
{
	/**
	 * Database manager instance.
	 *
	 * @var DatabaseManager
	 */
	protected $database;

	/**
	 * Create a new driver instance.
	 *
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		parent::__construct($config);
		$this->database = app('db')->connection($this->config('connection'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function create(array $params)
	{
		// Ensure the currency doesn't already exist
		if ($this->find($params['provider'], null) !== null) {
			return 'exists';
		}
		// Created at stamp
		$created = new DateTime('now');
		//default $params = ['provider', 'type']
		$params = array_merge([
			'config' => 'core.providers.'.$params['provider'],
			'icon' => $params['icon'] ?? null,
			'status' => 0,
			'created_at' => $created,
			'updated_at' => $created,
		], $params);

		return $this->database->table($this->config('table'))->insert($params);
	}

	/**
	 * {@inheritdoc}
	 */
	public function all()
	{
		$collection = new Collection($this->database->table($this->config('table'))->get());

		return $collection->keyBy('provider')
			->map(function ($item) {
				return [
					'id' => $item->id,
					'provider' => strtolower($item->provider),
					'type' => $item->type,
					'icon' => $item->icon,
					'status' => $item->status,
					'created_at' => $item->updated_at,
					'updated_at' => $item->updated_at,
				];
			})
			->all();
	}

	/**
	 * {@inheritdoc}
	 */
	public function find($provider, $status = 1)
	{
		$query = $this->database->table($this->config('table'))
			->where('provider', strtolower($provider));
		// Make status optional
		if (is_null($status) === false) {
			$query->where('status', $status);
		}

		return $query->first();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update($provider, array $attributes, DateTime $timestamp = null)
	{
		$table = $this->config('table');
		// Create timestamp
		if (empty($attributes['updated_at']) === true) {
			$attributes['updated_at'] = new DateTime('now');
		}

		return $this->database->table($table)
			->where('provider', strtolower($provider))
			->update($attributes);
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($provider)
	{
		$table = $this->config('table');

		return $this->database->table($table)
			->where('provider', strtolower($provider))
			->delete();
	}

	public function syncronize()
	{
		dd('provider');
	}
}
