<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;

class APICurrency extends Base
{
	public $provider;
	private $type = 'api';
	private $scope = 'api';
	public $status;
	public $key;
	public $title;

	public function __construct()
	{
		$config = config('core.providers.currencyapi');
		$this->provider = 'currencyapi';
		$this->status = $config['status'] ?? null;
		$this->key = $config['key'] ?? null;
		$this->title = $config['title'] ?? null;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch ($attribute) {
			case 'default':
				return;
			case 'id':
				return $this->fetchDatabase()->id;
			default:
				return;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		$self = new self();
		$config = [
			'provider' => $self->provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
			'key' => $values->key,
			'title' => $values->title
		];

		(new Base)->write($config);
		return new self();
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'key' => $this->key,
			'status' => $this->status,
			'title' => $this->title
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'key',
			'status',
		];
	}
}
