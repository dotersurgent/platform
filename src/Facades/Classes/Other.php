<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;
use SocialiteProviders\Manager\Config as SocialiteProvidersConfig;
use Carbon\Carbon;

class Other extends Base
{
	public $provider;
	private $type = 'other';
	private $scope = 'payment';
	public $status;
	public $icon;
	public $title;
	private $response;

	public function __construct($provider, $response = null)
	{
		$config = config("core.providers.{$provider}");
		$this->provider = $provider;
		$this->status = $config['status'] ?? null;
		$this->icon = $config['icon'] ?? null;
		$this->title = $config['title'] ?? null;
		$this->response = json_decode(unserialize($response)) ?? null;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch ($attribute) {
			case 'id':
				return $this->fetchDatabase()->id;
			case 'payment_id':
			return $this->getPaymentID();
			case 'payment_email':
				return '-';
			case 'payment_date':
				return $this->getPaymentDate() ?? null;
			default:
				return $this->additionalProviderConfig[$attribute] ?? null;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		// $provider = !empty($values->provider->provider) ? $values->provider->provider : $values->provider;
		$provider = $values->provider->provider ?? $values->provider;
		$self = new self($provider);
		$config = [
			'provider' => $provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
			'icon' => $values->icon,
			'title' => $values->title
		];

		(new Base)->write($config);
		return new self($provider);
	}

	public function delete($provider)
	{
		(new Base)->delete($provider);
		return true;
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'status' => $this->status,
			'additionalProviderConfig' => $this->additionalProviderConfig,
			'icon' => $this->icon,
			'title' => $this->title
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'status',
			'additional',
			'icon',
			'title'
		];
	}

	public function toConfig()
	{
		return new SocialiteProvidersConfig($this->client_id, $this->client_secret, $this->redirect);
	}

	public function getResponse($response)
	{
		return (new self($this->provider, $response));
	}

	private function getPaymentID()
	{
		return '-';
	}

	private function getPaymentEmail()
	{
		return '-';
	}

	public function getPaymentDate()
	{
		return !empty($this->response->payment_date) ? Carbon::parse($this->response->payment_date)
		->format('Y-m-d') : null;
	}
}
