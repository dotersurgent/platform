<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Modules\Provider\Entities\SiteProvider;

class APIMidtrans extends Base
{
	public $provider;
	private $type = 'api';
	private $scope = 'payment';
	public $status;
	public $client_id;
	public $client_secret;
	public $title;
	private $mode;
	private $images;

	public function __construct()
	{
		$config = config('core.providers.midtransapi');
		$this->provider = 'midtransapi';
		$this->status = $config['status'] ?? null;
		$this->client_id = $config['client_id'] ?? null;
		$this->client_secret = $config['client_secret'] ?? null;
		$this->mode = $config['mode'] ?? null;
		$this->images = SiteProvider::where('type', $this->type)
		->where('provider', $this->provider)->first()->images;
		$this->title = $config['title'] ?? null;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch ($attribute) {
			case 'isProduction':
				return $this->mode == 'sandbox' ? false : true;
			case 'id':
				return $this->fetchDatabase()->id;
			default:
				return;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		$self = new self();
		$media = SiteProvider::whereProvider($self->provider)
		->whereType('api')->first();
		if($values->hasFile('image'))
			$media = $media->uploadMedia($values->file('image'))->getUrl($media->getLibrary('thumb'));
		else{
			$media = $media->images;
		}
		
		$config = [
			'provider' => $self->provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
			'client_id' => $values->client_id,
			'client_secret' => $values->client_secret,
			'mode' => $values->mode ?? 'sandbox',
			'images' => $media,
			'title' => $values->title
		];

		(new Base)->write($config);
		return new self();
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'status' => $this->status,
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'mode' => $this->mode,
			'images' => $this->images,
			'title' => $this->title
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'client_id',
			'client_secret',
			'status',
			'mode',
			'image',
			'title'
		];
	}
}
