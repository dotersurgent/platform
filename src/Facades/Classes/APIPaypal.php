<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Modules\Provider\Entities\SiteProvider;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\WebProfile;
use PayPal\Api\ItemList;
use PayPal\Api\InputFields;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Details;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

use Modules\Transaction\Entities\Transaction as VisaTransaction;

class APIPaypal extends Base
{
	public $provider;
	private $type = 'api';
	private $scope = 'payment';
	public $status;
	public $client_id;
	public $client_secret;
	public $settings;
	public $title;
	private $images;
	private $ApiContext;
	private $response;

	public function __construct($response = null)
	{
		$config = config('core.providers.paypalapi');
		$this->provider = 'paypalapi';
		$this->status = $config['status'] ?? null;
		$this->client_id = $config['client_id'] ?? null;
		$this->client_secret = $config['client_secret'] ?? null;
		$this->settings = $config['settings'] ?? [];
		$this->title = $config['title'] ?? null;
		$this->images = SiteProvider::where('type', $this->type)
		->where('provider', $this->provider)->first()->images;
		$this->response = unserialize($response) ?? null;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch ($attribute) {
			case 'setting':
				return [
					'mode' => $this->mode,
					'http' => ['ConnectionTimeOut' => $this->timeout],
					'log' => [
						'LogEnabled' => $this->logEnabled,
						'file' => $this->logFileName,
						'FileName' => $this->logPath,
						'LogLevel' => 'ERROR'
					]
				];
			case 'id':
				return $this->fetchDatabase()->id;
			case 'payment_id':
				return $this->getPaymentID();
			case 'payment_email':
				return $this->getPaymentEmail();
			case 'payment_date':
				return $this->getPaymentDate() ?? null;
			case 'sale_id':
				return $this->getPaymentSaleID() ?? null;
			case 'mode':
				return $this->settings ? $this->settings['mode'] : '';
			case 'logEnabled':
				return $this->settings ? $this->settings['log']['LogEnabled'] : '';
			case 'logFileName':
				return $this->settings ? $this->settings['log']['file'] ?? '' : '';
			case 'timeout':
				return $this->settings ? $this->settings['http']['ConnectionTimeOut'] : '';
			case 'logPath':
				return storage_path().'/logs/paypal/'.$this->logFileName.'_'.Carbon::now()->toDateString().'.log';
			default:
				return;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		$self = new self();
		$media = SiteProvider::whereProvider($self->provider)
		->whereType('api')->first();
		if($values->hasFile('image'))
			$media = $media->uploadMedia($values->file('image'))->getUrl($media->getLibrary('thumb'));
		else
			$media = $media->images;

		$config = [
			'provider' => $self->provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
			'client_id' => $values->client_id,
			'client_secret' => $values->client_secret,
			'title' => $values->title,
			'settings' => [
				'mode' => $values->mode ?? 'sandbox',
				'http' => [
					'ConnectionTimeOut' => $values->timeout ?? 30
				],
				'log' => [
					'LogEnabled' => $values->logEnabled == '1' ? true : false,
					'file' => $values->logFileName,
					'FileName' => storage_path() . '/logs/' . ($values->logFileName ?? 'paypal') . '.log',
					'LogLevel' => 'ERROR'
				]
			],
			'images' => $media,
		];

		(new Base)->write($config);
		return new self();
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'status' => $this->status,
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'settings' => $this->settings,
			'title' => $this->title,
			'mode' => $this->mode,
			'timeout' => $this->timeout,
			'logEnabled' => $this->logEnabled,
			'logFileName' => $this->logFileName,
			'images' => $this->images
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'client_id',
			'client_secret',
			'status',
			'logEnabled',
			'logFileName',
			'timeout',
			'mode',
			'title',
			'image'
		];
	}

	private function connect()
	{
		$this->apiContext = new ApiContext(new OAuthTokenCredential(
			$this->client_id,
			$this->client_secret
		));

		$this->apiContext->setConfig($this->settings);
	}

	public function build($trx)
	{
		if($trx->payment_status == 'paid'){
			session()->flash('error_message', trans('Transaction has beed Paid.'));
			return redirect()->route('visas.submissions.edit', $trx->getKey());
		}
		$this->connect();
		# initialize the payer object and set the payment method to PayPal
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		// $shipment = session('shipping');
		// Session::forget('shipping');

		# need to update the order if the payment is complete, so we save it to the session
		session(['orderId' => $trx->getKey()]);
		
		$subtotal = currency($trx->visa_registration->service('price'), 'IDR', 'USD', false);
		$tax = toDecimal(0.1 * $subtotal);
		$total = $subtotal + $tax;
		# get all the items from the cart and parse the array into the Item object
		$items = [];
		$items[] = (new Item())
			->setName($trx->visa_registration->visa_master->code)
			->setCurrency('USD')
			->setQuantity(1)
			->setPrice($subtotal);

		# create a new item list and assign the items to it
		$itemList = new ItemList();
		$itemList->setItems($items);

		# Disable all irrelevant PayPal aspects in payment
		$inputFields = new InputFields();
		$inputFields->setAllowNote(true)
			->setNoShipping(1)
			->setAddressOverride(0);

		$webProfile = new WebProfile();
		$webProfile->setName(uniqid())
			->setInputFields($inputFields)
			->setTemporary(true);

		$createProfile = $webProfile->create($this->apiContext);

		# get the total price of the cart
		//additional details

		$details = new Details();
		$details
    ->setTax($tax)
    ->setSubtotal($subtotal);
    // ->setShipping($shipping);

		$amount = new Amount();
		$amount->setCurrency('USD')
			// ->setTotal(SCart::total() + $shipping)
			->setTotal($total)
			->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setItemList($itemList)
		->setDescription('Bali Visas Visa Submission');
			// ->setInvoiceNumber(uniqid());

		$redirectURLs = new RedirectUrls();
		$redirectURLs->setReturnUrl(route('checkout_status'))
		->setCancelUrl(route('visas.submissions.edit', $trx->getKey()));

		$payment = new Payment();
		$payment->setIntent('Sale')
		->setPayer($payer)
		->setRedirectUrls($redirectURLs)
		->setTransactions(array($transaction));
		$payment->setExperienceProfileId($createProfile->getId());

		try{
			$payment->create($this->apiContext);

			foreach ($payment->getLinks() as $link) {
				if ($link->getRel() == 'approval_url') {
					$redirectURL = $link->getHref();
					break;
				}
			}

			# store the payment ID into the session
			session(['paypalPaymentId' => $payment->getId()]);

			if (isset($redirectURL)) {
				return Redirect::away($redirectURL);
			}
		}catch(\Exception $e){
			session()->flash('checkout_message', [
				'status'=>'failed',
				'message'=>'There was a problem processing your payment. Please try again or contact support.'
			]);
			return redirect()->route('visas.submissions.edit', $trx->getKey());
		}
	}

	public function refund($response)
	{
		$saleId = '7D694046VN926045L';
		$amt = new Amount();
		$amt->setCurrency('USD')
    ->setTotal(0.01);

    $refundRequest = new RefundRequest();
		$refundRequest->setAmount($amt);

		$sale = new Sale();
		$sale->setId($saleId);

		try {
	    $this->connect();

	    $refundedSale = $sale->refundSale($refundRequest, $this->apiContext);
	    dd($refundedSale);
		} catch (Exception $ex) {
		    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
		    ResultPrinter::printError("Refund Sale", "Sale", null, $refundRequest, $ex);
		    exit(1);
		}
	}

	public function response(Request $req)
	{
		$paymentId = session('paypalPaymentId');
		$orderId = session('orderId');
		// $paymentId = 'PAYID-L7O7QXY3XJ686621H647191A';
		// $orderId = 'BV-VOA01-00003-1608345028';
		// dd($orderId);

		# now erase the payment ID from the session to avoid fraud
		session()->forget('paypalPaymentId');
		session()->forget('orderId');

		# Payment is processing but may still fail due e.g to insufficient funds
		$transaction = VisaTransaction::where((new VisaTransaction)->getKeyName(), $orderId)
		->firstorfail();

		# If the payer ID or token isn't set, there was a corrupt response and instantly abort
		if (empty($req->PayerID) || empty($req->token || empty($paymentId))) {
			session()->flash('error_message', 'There was a problem processing your payment. Please contact support.');
			return redirect()->route('visas.submissions.edit', $transaction->getKey());
		}

		$this->connect();

		$payment = Payment::get($paymentId, $this->apiContext);
		$execution = new PaymentExecution();
		$execution->setPayerId($req->PayerID);

		$result = $payment->execute($execution, $this->apiContext);

		if ($result->getState() == 'approved') {
			$transaction->update([
				'provider_id' => SiteProvider::where('provider', 'paypalapi')->first()->id,
				'payment_channel_payment_id' => $result->getId(),
				'payment_channel_response' => serialize($result),
				'payment_customer_id' => $result->getPayer()->getPayerInfo()->getPayerId(),
				'payment_customer_email' => $result->getPayer()->getPayerInfo()->getEmail(),
				'payment_currency' => $result->transactions[0]->getAmount()->getCurrency(),
				'payment_country' => $result->getPayer()->getPayerInfo()->getCountryCode(),
				'payment_amount' => $result->transactions[0]->getAmount()->getTotal(),
				'payment_status' => 'paid'
			]);

			$transaction->visa_registration()->update([
				'status_id' => '2'
			]);

			session()->forget('payment_gate');
		}else{
			$transaction->update([
				'payment_status' => 'pending'
			]);
			session()->flash('checkout_message', [
				'status'=>'failed',
				'message'=>'There was a problem processing your payment. Please contact support.'
			]);
		}
		// event(new Notifications('order'));
		session()->flash('success_message', trans('Transaction Success.'));
		return redirect()->route('visas.submissions.edit', $transaction->getKey());
	}

	public function getResponse($response)
	{
		return (new self($response));
	}

	private function getPaymentID()
	{
		return $this->response->getId();
	}

	private function getPaymentEmail()
	{
		return $this->response->getPayer()->getPayerInfo()->getEmail();
	}

	private function getPaymentDate()
	{
		return Carbon::parse($this->response->transactions[0]->getRelatedResources()[0]->getSale()->create_time)
		->format('Y-m-d');
	}

	private function getPaymentSaleID()
	{
		return $this->response->transactions[0]->getRelatedResources()[0]->getSale()->getId();
	}
}
