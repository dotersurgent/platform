<?php

namespace Gdoters\Platform\Facades\Classes;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;

class Base implements Arrayable
{
	public function write(Array $array)
	{
		$providers = [];
		foreach (\Arr::except($array, ['provider', '_token', 'method']) as $key => $value) {
			$providers['core.providers.'.$array['provider'].'.'.$key] = $value;
		}

		config($providers);

		$fp = fopen(base_path() .'/config/core/providers.php' , 'w');
		fwrite($fp, '<?php return ' . var_export(config("core.providers"), true) . ';');
		fclose($fp);

		return true;
	}

	public function delete($keyName)
	{
		$providers = config("core.providers");
		unset($providers[$keyName]);

		$fp = fopen(base_path() .'/config/core/providers.php' , 'w');
		fwrite($fp, '<?php return ' . var_export($providers, true) . ';');
		fclose($fp);

		return true;
	}

	public function toArray()
	{
		return [];
	}

	public function toDatabase()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'config' => 'core.providers.'.$this->provider,
			'icon' => $this->icon,
			'status' => $this->status
		];
	}

	public function fetchDatabase()
	{
		return \Provider::find($this->provider, $this->status);
	}
}