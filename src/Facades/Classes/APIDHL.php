<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;
use Modules\Provider\Entities\SiteProvider;

class APIDHL extends Base
{
	public $provider;
	private $type = 'api';
	private $scope = 'packaging';
	private $images;
	public $status;
	public $id;
	public $password;
	public $account;
	public $account_shipper;
	public $account_billing;
	public $account_dutiable;
	public $company_name;
	public $addressline1;
	public $addressline2;
	public $city;
	public $division;
	public $division_code;
	public $postal;
	public $country_code;
	public $country_name;
	public $contact_name;
	public $contact_phone;
	public $contact_phone_ext;
	public $contact_fax;
	public $contact_telex;
	public $contact_email;

	public function __construct()
	{
		$config = config('core.providers.dhlapi');
		$this->provider = 'dhlapi';
		$this->status = $config['status'] ?? null;
		$this->id = $config['id'] ?? null;
		$this->password = $config['password'] ?? null;
		$this->account = $config['account'] ?? null;
		$this->account_shipper = $config['account_shipper'] ?? null;
		$this->account_billing = $config['account_billing'] ?? null;
		$this->account_dutiable = $config['account_dutiable'] ?? null;
		$this->company_name = $config['company_name'] ?? null;
		$this->addressline1 = $config['addressline1'] ?? null;
		$this->addressline2 = $config['addressline2'] ?? null;
		$this->city = $config['city'] ?? null;
		$this->division = $config['division'] ?? null;
		$this->division_code = $config['division_code'] ?? null;
		$this->postal = $config['postal'] ?? null;
		$this->country_code = $config['country_code'] ?? null;
		$this->country_name = $config['country_name'] ?? null;
		$this->contact_name = $config['contact_name'] ?? null;
		$this->contact_phone = $config['contact_phone'] ?? null;
		$this->contact_phone_ext = $config['contact_phone_ext'] ?? null;
		$this->contact_fax = $config['contact_fax'] ?? null;
		$this->contact_telex = $config['contact_telex'] ?? null;
		$this->contact_email = $config['contact_email'] ?? null;
		$this->images = SiteProvider::whereType($this->type)
		->whereProvider($this->provider)->first()->images;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch ($attribute) {
			case 'default':
				return;
			case 'id':
				return $this->fetchDatabase()->id;
			default:
				return;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		$self = new self();
		$media = SiteProvider::whereProvider($self->provider)
		->whereType('api')->first();
		if($values->hasFile('image'))
			$media = $media->uploadMedia($values->file('image'))->getUrl($media->getLibrary('thumb'));
		else
			$media = $media->images;

		$config = [
			'provider' => $self->provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
		  'id' => $values->id,
			'password' => $values->password,
			'account' => $values->account,
			'account_shipper' => $values->account_shipper,
			'account_billing' => $values->account_billing,
			'account_dutiable' => $values->account_dutiable,
			'company_name' => $values->company_name,
			'addressline1' => $values->addressline1,
			'addressline2' => $values->addressline2,
			'city' => $values->city,
			'division' => $values->division,
			'division_code' => $values->division_code,
			'postal' => $values->postal,
			'country_code' => $values->country_code,
			'country_name' => $values->country_name,
			'contact_name' => $values->contact_name,
			'contact_phone' => $values->contact_phone,
			'contact_phone_ext' => $values->contact_phone_ext,
			'contact_fax' => $values->contact_fax,
			'contact_telex' => $values->contact_telex,
			'contact_email' => $values->contact_email,
			'images' => $media
		];

		(new Base)->write($config);
		return new self();
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'status' => $this->status,
			'id' => $this->id,
			'password' => $this->password,
			'account' => $this->account,
			'account_shipper' => $this->account_shipper,
			'account_billing' => $this->account_billing,
			'account_dutiable' => $this->account_dutiable,
			'company_name' => $this->company_name,
			'addressline1' => $this->addressline1,
			'addressline2' => $this->addressline2,
			'city' => $this->city,
			'division' => $this->division,
			'division_code' => $this->division_code,
			'postal' => $this->postal,
			'country_code' => $this->country_code,
			'country_name' => $this->country_name,
			'contact_name' => $this->contact_name,
			'contact_phone' => $this->contact_phone,
			'contact_phone_ext' => $this->contact_phone_ext,
			'contact_fax' => $this->contact_fax,
			'contact_telex' => $this->contact_telex,
			'contact_email' => $this->contact_email,
			'images' => $this->images
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'status',
			'id',
			'password',
			'account',
			'account_shipper',
			'account_billing',
			'account_dutiable',
			'company_name',
			'addressline1',
			'addressline2',
			'city',
			'division',
			'division_code',
			'postal',
			'country_code',
			'country_name',
			'contact_name',
			'contact_phone',
			'contact_phone_ext',
			'contact_fax',
			'contact_telex',
			'contact_email',
			'image'
		];
	}
}
