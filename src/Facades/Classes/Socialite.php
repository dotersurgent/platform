<?php

namespace Gdoters\Platform\Facades\Classes;

use Gdoters\Platform\Facades\Classes\Base;
use Illuminate\Http\Request;
use SocialiteProviders\Manager\Config as SocialiteProvidersConfig;

class Socialite extends Base
{
	public $provider;
	private $type = 'socialite';
	private $scope = 'login';
	public $status;
	public $client_id;
	public $client_secret;
	public $redirect;
	public $additionalProviderConfig;
	public $icon;
	public $title;

	public function __construct($provider)
	{
		$config = config("core.providers.{$provider}");
		$this->provider = $provider;
		$this->status = $config['status'] ?? null;
		$this->client_id = $config['client_id'] ?? null;
		$this->client_secret = $config['client_secret'] ?? null;
		$this->redirect = $config['redirect'] ?? null;
		$this->additionalProviderConfig = $config['additionalProviderConfig'] ?? '';
		$this->icon = $config['icon'] ?? null;
		$this->title = $config['title'] ?? null;
	}

	public function __get($attribute)
	{
		if (property_exists($this, $attribute)) {
			return $this->{$attribute};
		}

		switch (true) {
			case $attribute == 'default':
				return;
			case 'id':
			case 'payment_id':
			case 'payment_email':
				return '-';
			case 'payment_date':
				return null;
			default:
				return $this->additionalProviderConfig[$attribute] ?? null;
		}
	}

	public function setStatus(bool $status)
	{
		$config = [
			'provider' => $this->provider,
			'status' => $status,
		];

		return (new Base)->write($config);
	}

	public static function setAttributes(Request $values)
	{
		// $provider = !empty($values->provider->provider) ? $values->provider->provider : $values->provider;
		$provider = $values->provider->provider ?? $values->provider;
		$self = new self($provider);
		$config = [
			'provider' => $provider,
			'type' => $self->type,
			'scope' => $self->scope,
			'status' => $values->status == '1' ? true : false,
			'client_id' => $values->client_id,
			'client_secret' => $values->client_secret,
			'redirect' => route('social.callback', $provider),
			'icon' => $values->icon,
			'additionalProviderConfig' => $values->additionalProviderConfig ?? [],
			'title' => $values->title
		];

		(new Base)->write($config);
		return new self($provider);
	}

	public function delete($provider)
	{
		(new Base)->delete($provider);
		return true;
	}

	public function toArray()
	{
		return [
			'provider' => $this->provider,
			'type' => $this->type,
			'scope' => $this->scope,
			'status' => $this->status,
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'redirect' => $this->redirect,
			'additionalProviderConfig' => $this->additionalProviderConfig,
			'icon' => $this->icon,
			'title' => $this->title
		];
	}

	public function getParameters()
	{
		return [
			'provider',
			'type',
			'status',
			'client_id',
			'client_secret',
			'additional',
			'icon',
			'title'
		];
	}

	public function toConfig()
	{
		return new SocialiteProvidersConfig($this->client_id, $this->client_secret, $this->redirect);
	}

	public function getResponse($response)
	{
		return (new self(''));
	}

	private function getPaymentID()
	{
		return '-';
	}

	private function getPaymentEmail()
	{
		return '-';
	}

	private function getPaymentDate()
	{
		return null;
	}
}
