<?php

namespace Gdoters\Platform;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class ProviderServiceProvider extends ServiceProvider
{
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerProvider();

		if ($this->app->runningInConsole()) {
			$this->registerResources();
			$this->registerProviderCommands();
		}
	}

	/**
	 * Register provider provider.
	 *
	 * @return void
	 */
	public function registerProvider()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/../config/core/provider.php', 'core.provider'
		);

		$this->app->singleton('provider', function ($app) {
			return new Provider(
				config('core.provider'),
				$app['cache']
			);
		});
	}

	/**
	 * Register provider resources.
	 *
	 * @return void
	 */
	public function registerResources()
	{
		if ($this->isLumen() === false) {
			$this->publishes([
				__DIR__ . '/../config/core/provider.php' => config_path('core/provider.php'),
				__DIR__ . '/../resources/providers.php' => config_path('core/providers.php'),
			], 'config');
		}

		$this->publishes([
			__DIR__ . '/../database/migrations/provider' => base_path('/database/migrations'),
		], 'migrations');
	}

	/**
	 * Register provider commands.
	 *
	 * @return void
	 */
	public function registerProviderCommands()
	{
		$this->commands([
			Console\Provider\Cleanup::class,
			Console\Provider\Manage::class,
			// Console\Provider\Update::class,
		]);
	}

	/**
	 * Check if package is running under Lumen app
	 *
	 * @return bool
	 */
	protected function isLumen()
	{
		return Str::contains($this->app->version(), 'Lumen') === true;
	}
}
