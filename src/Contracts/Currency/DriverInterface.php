<?php

namespace Gdoters\Platform\Contracts\Currency;

use DateTime;
use Illuminate\Http\Request;

interface DriverInterface
{
	/**
	 * Create a new resources.
	 *
	 * @param array $params
	 *
	 * @return bool
	 */
	public function create(array $params);

	/**
	 * Get all resources.
	 *
	 * @return array
	 */
	public function all();

	/**
	 * Get given resources from storage.
	 *
	 * @param string $code
	 * @param int    $active
	 *
	 * @return mixed
	 */
	public function find($code, $active = 1);

	/**
	 * Update given resources.
	 *
	 * @param string   $code
	 * @param array    $attributes
	 * @param DateTime $timestamp
	 *
	 * @return int
	 */
	public function update($code, array $attributes, DateTime $timestamp = null);

	/**
	 * Remove given resources from storage.
	 *
	 * @return int
	 */
	public function delete($code);

	/**
	 * Syncronize Data.
	 *
	 * @return int
	 */
	public function syncronize();
}