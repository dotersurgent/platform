<?php

namespace Gdoters\Platform;

use Closure;
use Illuminate\Http\Request;
use Gdoters\Platform\Facades\Classes\APIGoogleApi;
use Gdoters\Platform\Facades\Classes\APICurrency;
use Gdoters\Platform\Facades\Classes\APIDHL;
use Gdoters\Platform\Facades\Classes\APIPaypal;
use Gdoters\Platform\Facades\Classes\APIMidtrans;
use Gdoters\Platform\Facades\Classes\Socialite;
use Gdoters\Platform\Facades\Classes\Other;
use Modules\Provider\Entities\SiteProvider;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Cache\Factory as FactoryContract;
use Carbon\Carbon;

class Provider
{
	/**
	 * Currency configuration.
	 *
	 * @var array
	 */
	protected $config = [];

	/**
	 * Laravel application
	 *
	 * @var \Illuminate\Contracts\Cache\Factory
	 */
	protected $cache;

	/**
	 * Currency driver instance.
	 *
	 * @var Contracts\DriverInterface
	 */
	protected $driver;

	/**
	 * Cached currencies
	 *
	 * @var array
	 */
	protected $providers_cache;

	/**
	 * Create a new instance.
	 *
	 * @param array           $config
	 * @param FactoryContract $cache
	 */

	public function __construct(array $config, FactoryContract $cache)
	{
		$this->config = $config;
		$this->cache = $cache->store($this->config('cache_driver'));
	}

	protected $provider;
	protected function getContent($provider)
	{
		switch (true) {
			case $provider == 'paypalapi':
				$this->provider = new APIPaypal();
				break;
			case $provider == 'midtransapi':
				$this->provider = new APIMidtrans();
				break;
			case $provider == 'googleapi':
				$this->provider = new APIGoogleApi();
				break;
			case $provider == 'dhlapi':
				$this->provider = new APIDHL();
				break;
			case $provider == 'currencyapi':
				$this->provider = new APICurrency();
				break;
			case $provider == 'cash' || $provider == 'pay_after_done' || $provider == 'bank_transfer':
				$this->provider = new Other($provider);
				break;
			default:
				$this->provider = new Socialite($provider);
				break;
		}

		return $this->provider;
	}

	public function get($provider)
	{
		return $this->getContent($provider);
	}

	public function set($provider, $values)
	{
		try{
			$item = $this->getContent($provider->provider)->setAttributes($values);
		}catch(Exception $e){
			$msg = json_decode($e->getMessage());
			//catch message exception
			if(is_object($msg)){
				return $msg->message;
			}
			return $e->getMessage();
		}

		return $this->getContent($provider->provider);
	}

	public function delete($provider)
	{
		return $this->getContent($provider)->delete($provider);
	}

	public function sync()
	{
		$providers = [
			'paypalapi' => 'api',
			'currencyapi' => 'api',
			'midtransapi' => 'api', 
			'bank_transfer' => 'other',
			'pay_after_done' => 'other',
			'cash' => 'other'
		];
		foreach ($providers as $key => $value) {
			$prv = SiteProvider::whereProvider($key)
			->whereType($value)
			->first();

			if(is_null($prv)){
				SiteProvider::Create([
					'provider' => $key,
					'type' => $value,
					'config' => "core.providers.{$value}"
				]);
			}
		}

		foreach (SiteProvider::pluck('provider') as $key => $value) {
			self::getContent($value)->setAttributes(new Request(self::get($value)->toArray()));
		}
	}

	public function scope($scope, array $filter = null)
	{
		return collect(config('core.providers'))
		->filter(function($value, $key) use ($scope){
			return isset($value['scope']) && $value['scope'] == $scope;
		})->map(function($value, $key) use ($filter){
			$value = collect(self::getContent($key));
			if($filter)
				$value = $value->only($filter);
			return $value->toArray();
		})->all();
	}

	/**
	 * Determine if the provided currency is valid.
	 *
	 * @param string $provider
	 *
	 * @return array|null
	 */
	public function hasProvider($provider)
	{
		return array_key_exists(strtoupper($provider), $this->getCurrencies());
	}

	/**
	 * Determine if the provided currency is active.
	 *
	 * @param string $provider
	 *
	 * @return bool
	 */
	public function isActive($provider)
	{
		return $provider && (bool) Arr::get($this->getCurrency($provider), 'active', false);
	}

	/**
	 * Return the current currency if the
	 * one supplied is not valid.
	 *
	 * @param string $provider
	 *
	 * @return array|null
	 */
	public function getCurrency($provider = null)
	{
		$provider = $provider ?: $this->getUserCurrency();

		return Arr::get($this->getCurrencies(), strtoupper($provider));
	}

	/**
	 * Return all currencies.
	 *
	 * @return array
	 */
	public function getCurrencies()
	{
		if ($this->providers_cache === null) {
			if (config('app.debug', false) === true) {
				$this->providers_cache = $this->getDriver()->all();
			} else {
				$this->providers_cache = $this->cache->rememberForever('torann.currency', function () {
					return $this->getDriver()->all();
				});
			}
		}

		return $this->providers_cache;
	}

	/**
	 * Return all active currencies.
	 *
	 * @return array
	 */
	public function getActiveCurrencies()
	{
		return array_filter($this->getCurrencies(), function ($currency) {
			return $currency['active'] == true;
		});
	}

	/**
	 * Get storage driver.
	 *
	 * @return \Gdoters\Platform\Contracts\Currency\DriverInterface
	 */
	public function getDriver()
	{
		if ($this->driver === null) {
			// Get driver configuration
			$config = $this->config('drivers.' . $this->config('driver'), []);

			// Get driver class
			$driver = Arr::pull($config, 'class');

			// Create driver instance
			$this->driver = new $driver($config);
		}

		return $this->driver;
	}

	/**
	 * Get formatter driver.
	 *
	 * @return \Gdoters\Platform\Contracts\FormatterInterface
	 */
	public function getFormatter()
	{
		if ($this->formatter === null && $this->config('formatter') !== null) {
			// Get formatter configuration
			$config = $this->config('formatters.' . $this->config('formatter'), []);

			// Get formatter class
			$class = Arr::pull($config, 'class');

			// Create formatter instance
			$this->formatter = new $class(array_filter($config));
		}

		return $this->formatter;
	}

	/**
	 * Clear cached currencies.
	 */
	public function clearCache()
	{
		$this->cache->forget('torann.currency');
		$this->providers_cache = null;
	}

	/**
	 * Get configuration value.
	 *
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	public function config($key = null, $default = null)
	{
		if ($key === null) {
			return $this->config;
		}

		return Arr::get($this->config, $key, $default);
	}

	/**
	 * Get the given property value from provided currency.
	 *
	 * @param string $provider
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return array
	 */
	protected function getCurrencyProp($provider, $key, $default = null)
	{
		return Arr::get($this->getCurrency($provider), $key, $default);
	}

	/**
	 * Get a given value from the current currency.
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function __get($key)
	{
		return Arr::get($this->getCurrency(), $key);
	}

	/**
	 * Dynamically call the default driver instance.
	 *
	 * @param string $method
	 * @param array  $parameters
	 *
	 * @return mixed
	 */
	public function __call($method, $parameters)
	{
		return call_user_func_array([$this->getDriver(), $method], $parameters);
	}
}
