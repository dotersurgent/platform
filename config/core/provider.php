<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Application Provider
	|--------------------------------------------------------------------------
	|
	| The application provider determines the default providers that will be
	| used by the provider service provider. You are free to set this value
	| to any of the provider which will be supported by the application.
	|
	*/

	'enabled' => true,

	/*
	|--------------------------------------------------------------------------
	| Default Storage Driver
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default storage driver that should be used
	| by the framework.
	|
	| Supported: "database", "filesystem"
	|
	*/

	'driver' => 'database',

	/*
	|--------------------------------------------------------------------------
	| Default Storage Driver
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default cache driver that should be used
	| by the framework.
	|
	| Supported: all cache drivers supported by Laravel
	|
	*/

	'cache_driver' => null,

	/*
	|--------------------------------------------------------------------------
	| Storage Specific Configuration
	|--------------------------------------------------------------------------
	|
	| Here you may configure as many storage drivers as you wish.
	|
	*/

	'drivers' => [

		'database' => [
			'class' => \Gdoters\Platform\Drivers\Provider\Database::class,
			'connection' => null,
			'table' => 'providers',
		],

		'filesystem' => [
			'class' => \Gdoters\Platform\Drivers\Provider\Filesystem::class,
			'disk' => null,
			'path' => config_path('core/providers.php'),
		],

	],

	'providers' => [
		'googleapi' => \Gdoters\Platform\Facades\Classes\APIGoogleApi::class,
		'currencyapi' => \Gdoters\Platform\Facades\Classes\APICurrency::class,
		'midtransapi' => \Gdoters\Platform\Facades\Classes\APIMidtrans::class,
		'paypalapi' => \Gdoters\Platform\Facades\Classes\APIPaypal::class,
		'bank_transfer' => \Gdoters\Platform\Facades\Classes\Other::class,
		'cash' => \Gdoters\Platform\Facades\Classes\Other::class,
		'pay_after_done' => \Gdoters\Platform\Facades\Classes\Other::class,
		// 'facebook' => \Gdoters\Platform\Facades\Classes\Socialite::class,
	]
];