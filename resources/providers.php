<?php

/*
  |--------------------------------------------------------------------------
  | Provider Configuration
  |--------------------------------------------------------------------------
  |
  | Here you may configure as many providers as you wish.
  |
  */
return [
	'googleapi' => [
    'type' => 'api',
    'scope' => 'api',
    'status' => false,
    'key' => 'YOUR_CLIENT_KEY',
    'title' => 'Google Map API',
  ],
  'currencyapi' => [
    'type' => 'api',
    'scope' => 'api',
    'status' => false,
    'key' => 'YOUR_CLIENT_KEY',
    'title' => 'Currency API',
  ],
  'midtransapi' => [
    'type' => 'api',
    'scope' => 'payment',
    'status' => false,
    'client_id' => 'YOUR_CLIENT_ID',
    'client_secret' => 'YOUR_CLIENT_SECRET',
    'mode' => 'sandbox',
    'images' => '#',
    'title' => 'Midtrans',
  ],
  'paypalapi' => [
    'type' => 'api',
    'scope' => 'payment',
    'status' => false,
    'client_id' => 'YOUR_CLIENT_ID',
    'client_secret' => 'YOUR_CLIENT_SECRET',
    'settings' => [
      'mode' => 'sandbox',
      'http' => [
        'ConnectionTimeOut' => '30',
      ],
      'log' => [
        'LogEnabled' => false,
        'file' => 'paypal',
        'FileName' => storage_path('logs/paypal.log'),
        'LogLevel' => 'ERROR',
      ],
    ],
    'images' => '#',
    'title' => 'PayPal',
  ],
  'bank_transfer' => [
    'type' => 'other',
    'scope' => 'payment',
    'status' => false,
    'icon' => NULL,
    'title' => 'Bank Transfer',
  ],
  'cash' => [
    'type' => 'other',
    'scope' => 'payment',
    'status' => true,
    'icon' => NULL,
    'title' => 'Cash',
  ],
  'pay_after_done' => [
    'type' => 'other',
    'scope' => 'payment',
    'status' => true,
    'icon' => NULL,
    'title' => 'Pay After Done',
  ],
];