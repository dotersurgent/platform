<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderTable extends Migration
{
  /**
   * Currencies table name
   *
   * @var string
   */
  protected $table_name;

  /**
   * Create a new migration instance.
   */
  public function __construct()
  {
    $this->table_name = config('core.provider.drivers.database.table');
  }

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create($this->table_name, function (Blueprint $table) {
			$table->increments('id');
      $table->string('provider');
      $table->enum('type', ['socialite','api','other']);
      $table->string('config')->nullable();
      $table->string('icon')->nullable();
      $table->boolean('status')->default(0);
      $table->timestamps();
    });

    DB::table($this->table_name)
    	->insert(
    		array(
          array(
            'provider'=>'googleapi',
            'config'=>'core.providers.googleapi',
            'type'=>'api',
            'icon'=>null,
            'status'=>false,
            'created_at'=> NOW(),
            'updated_at'=> NOW()
          ),
          array(
            'provider'=>'currencyapi',
            'config' =>'core.providers.currencyapi',
            'type'=>'api',
            'icon'=>null,
            'status'=>false,
            'created_at'=> NOW(),
            'updated_at'=> NOW()
          ),
          array(
            'provider'=>'midtransapi',
            'config'=>'core.providers.midtransapi',
            'type'=>'api',
            'icon'=>null,
            'status'=>false,
            'created_at'=> NOW(),
            'updated_at'=> NOW()
          ),
          array(
            'provider'=>'paypalapi',
            'config'=>'core.providers.paypalapi',
            'type'=>'api',
            'icon'=>null,
            'status'=>false,
            'created_at'=> NOW(),
            'updated_at'=> NOW()
          )
        )
  	);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::disableForeignKeyConstraints();
    Schema::dropIfExists($this->table_name);
    Schema::enableForeignKeyConstraints();
	}
}
